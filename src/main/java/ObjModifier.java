import java.nio.FloatBuffer;

public class ObjModifier {
    public static void centerVertices(FloatBuffer buf) {
        // We often get 3D models which are not positioned at the origin of the coordinate system. Placing such models
        // on a 3D map is harder for users. One solution is to reposition such a model so that its center is at the
        // origin of the coordinate system. Your task is to implement the centering.
        //
        // FloatBuffer stores the vertices of a mesh in a continuous array of floats (see below)
        // [x0, y0, z0, x1, y1, z1, ..., xn, yn, zn]
        // This kind of layout is common in low-level 3D graphics APIs.

        float[] min = new float[3];
        float[] max = new float[3];

        for (int i = 0; i < buf.capacity(); i += 3) {
            if (i == 0) {
                for (int j = 0; j < 3; ++ j) {
                    min [j] = buf.get(i + j);
                    max [j] = buf.get(i + j);
                }
            }

            else {
                for (int j = 0; j < 3; ++ j) {
                    if (buf.get(i + j) < min [j]) {
                        min [j] = buf.get(i + j);
                    } else if (buf.get(i + j) > max [j]) {
                        max [j] = buf.get(i + j);
                    }
                }
            }
        }

        float[] center = new float[3];
        for (int i = 0; i < 3; ++ i) {
            center[i] = min [i] + ((max[i] - min [i]) / 2);
        }

        for (int i = 0; i < buf.capacity(); i += 3) {
            for (int j = 0; j < 3; ++ j) {
                buf.put (i + j, buf.get(i + j) - center [j]);
            }
        }
    }
}
